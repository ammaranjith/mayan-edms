# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('checkouts', '0004_auto_20150617_0330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentcheckout',
            name='expiration_datetime',
            field=models.DateTimeField(default=datetime.datetime(2115, 12, 15, 12, 16, 10, 390965), help_text='Amount of time to hold the document checked out in minutes.', verbose_name='Check out expiration date and time'),
            preserve_default=True,
        ),
    ]
