# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('checkouts', '0005_auto_20160108_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentcheckout',
            name='expiration_datetime',
            field=models.DateTimeField(default=datetime.datetime(2115, 12, 15, 12, 37, 4, 499034), help_text='Amount of time to hold the document checked out in minutes.', verbose_name='Check out expiration date and time'),
            preserve_default=True,
        ),
    ]
