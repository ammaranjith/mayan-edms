# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document_states', '0003_workflowtransition_users'),
        ('tags', '0006_documenttag'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='workflow_transition',
            field=models.ForeignKey(related_name='transition_tags', verbose_name='Workflow Transition', to='document_states.WorkflowTransition', null=True),
            preserve_default=True,
        ),
    ]
