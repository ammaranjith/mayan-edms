from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from events.classes import Event

event_metadata_edit = Event(
    name='metadata_edit', label=_('Metadata edited')
)
event_metadata_add = Event(
    name='metadata_add', label=_('Metadata added')
)
event_metadata_remove = Event(
    name='metadata_remove', label=_('Metadata removed')
)
