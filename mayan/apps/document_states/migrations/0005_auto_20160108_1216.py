# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document_states', '0004_workflowtransition_tag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='workflowstate',
            name='completion',
        ),
        migrations.AddField(
            model_name='workflowstate',
            name='final',
            field=models.BooleanField(default=False, help_text='Select if this will be the state with which you want the workflow to end in. Only one state can be the final state.', verbose_name='Initial'),
            preserve_default=True,
        ),
    ]
