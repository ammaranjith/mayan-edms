# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0008_remove_tag_workflow_transition'),
        ('document_states', '0003_workflowtransition_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='workflowtransition',
            name='tag',
            field=models.ForeignKey(related_name='tag_transitions', verbose_name='Transition Tag', to='tags.Tag', null=True),
            preserve_default=True,
        ),
    ]
