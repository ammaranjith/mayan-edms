from __future__ import unicode_literals
import logging

from .models import Workflow, WorkflowInstanceLogEntry
from .tasks import task_init_unassigned_workflow_document_state

logger = logging.getLogger(__name__)


def launch_workflow(sender, instance, created, **kwargs):
    if created:
        Workflow.objects.launch_for(instance)

def remove_document_tags_on_transition_deletion(sender, instance, **kwargs):
    transition = instance
    log_entries = WorkflowInstanceLogEntry.objects.filter(transition=transition)
    for entry in log_entries:
        if entry.workflow_instance.get_last_transition() == entry.transition:
            document = entry.workflow_instance.document

            for old_tag in document.attached_tags().all():
                old_tag.documents.remove(document)

def initialize_state_unassigned_workflow_documents(sender=None, instance=None, pk_set=None, action="post_add", **kwargs):
    logger.debug('workflow instance: %s', instance)
    workflow = instance

    task_init_unassigned_workflow_document_state.apply_async(
        kwargs={
            'workflow_id': instance.pk,
            'document_types_pk_set': pk_set,
        }
    )

