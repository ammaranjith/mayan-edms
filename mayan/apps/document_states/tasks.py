import logging

from mayan.celery import app
from documents.models import Document
from document_states.models import Workflow, WorkflowInstance

@app.task(ignore_result=True)
def task_init_unassigned_workflow_document_state(workflow_id=None, document_types_pk_set=None):

    try:
        workflow = Workflow.objects.get(pk=workflow_id)
        for document_type_pk in document_types_pk_set:
            docs = Document.objects.filter(document_type__id=document_type_pk)
            for doc in docs:
                if not WorkflowInstance.objects.filter(workflow=workflow, document=doc).exists():
                    workflow.launch_for(doc)
    except:
        pass



