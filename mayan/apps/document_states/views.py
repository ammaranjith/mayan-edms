from __future__ import absolute_import, unicode_literals

from django.contrib import messages
from django.conf import settings
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView
from django.contrib.auth.models import Group, User
from django.db.models import Q
import logging

from notifications import notify
from notifications.models import Notification
from actstream.actions import follow, unfollow
from actstream.models import followers

from acls.models import AccessControlList
from common.views import (
    AssignRemoveView, SingleObjectCreateView, SingleObjectDeleteView,
    SingleObjectEditView, SingleObjectListView
)
from documents.models import Document, DocumentType
from documents.views import DocumentListView
from documents.events import event_document_state_transitioned
from permissions import Permission
from events.classes import Event

from .forms import (
    WorkflowForm, WorkflowInstanceTransitionForm, WorkflowStateForm,
    WorkflowTransitionForm
)
from .models import Workflow, WorkflowInstance, WorkflowState, WorkflowTransition
from .permissions import (
    permission_workflow_create, permission_workflow_delete,
    permission_workflow_edit, permission_workflow_transition,
    permission_workflow_view,
)

logger = logging.getLogger(__name__)

class DocumentWorkflowInstanceListView(SingleObjectListView):
    def dispatch(self, request, *args, **kwargs):
        try:
            Permission.check_permissions(
                request.user, (permission_workflow_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_workflow_view, request.user,
                self.get_document()
            )

        return super(
            DocumentWorkflowInstanceListView, self
        ).dispatch(request, *args, **kwargs)

    def get_document(self):
        return get_object_or_404(Document, pk=self.kwargs['pk'])

    def get_extra_context(self):
        return {
            'hide_link': True,
            'object': self.get_document(),
            'title': _(
                'Workflows for document: %s'
            ) % self.get_document(),
        }

    def get_queryset(self):
        return self.get_document().workflows.all()


class WorkflowDocumentListView(DocumentListView):
    def dispatch(self, request, *args, **kwargs):
        self.workflow = get_object_or_404(Workflow, pk=self.kwargs['pk'])

        try:
            Permission.check_permissions(
                request.user, (permission_workflow_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_workflow_view, request.user, self.workflow
            )

        return super(
            WorkflowDocumentListView, self
        ).dispatch(request, *args, **kwargs)

    def get_document_queryset(self):
        return Document.objects.filter(
            document_type__in=self.workflow.document_types.all()
        )

    def get_extra_context(self):
        return {
            'hide_links': True,
            'object': self.workflow,
            'title': _('Documents with the workflow: %s') % self.workflow
        }


class WorkflowInstanceDetailView(SingleObjectListView):
    def dispatch(self, request, *args, **kwargs):
        try:
            Permission.check_permissions(
                request.user, (permission_workflow_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_workflow_view, request.user,
                self.get_workflow_instance().document
            )

        return super(
            WorkflowInstanceDetailView, self
        ).dispatch(request, *args, **kwargs)

    def get_extra_context(self):
        return {
            'hide_object': True,
            'navigation_object_list': ('object', 'workflow_instance'),
            'object': self.get_workflow_instance().document,
            'title': _('Detail of workflow: %(workflow)s') % {
                'workflow': self.get_workflow_instance()
            },
            'workflow_instance': self.get_workflow_instance(),
        }

    def get_queryset(self):
        return self.get_workflow_instance().log_entries.order_by('-datetime')

    def get_workflow_instance(self):
        return get_object_or_404(WorkflowInstance, pk=self.kwargs['pk'])


class WorkflowInstanceTransitionView(FormView):
    form_class = WorkflowInstanceTransitionForm
    template_name = 'appearance/generic_form.html'

    def dispatch(self, request, *args, **kwargs):
        workflow_instance = self.get_workflow_instance()
        try:
            Permission.check_permissions(
                request.user, (permission_workflow_transition,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_workflow_transition, request.user,
                self.get_workflow_instance().document
            )

        if not workflow_instance.workflow.document_types.count():
            messages.error(
                self.request, _('The document type of this document was associated with ' + unicode(workflow_instance.workflow) + ' workflow. Re-associate it back to do any transitions.')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )


        return super(
            WorkflowInstanceTransitionView, self
        ).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        transition = self.get_workflow_instance().workflow.transitions.get(
            pk=form.cleaned_data['transition']
        )

        #mark notifications associated with the current transition as read
        document = self.get_workflow_instance().document

        last_transition = self.get_workflow_instance().get_last_transition()
        if last_transition:
            qs = Notification.objects.unread().filter(action_object_object_id=last_transition.pk,
                    target_object_id=document.pk)
            if len(qs):
                qs.mark_all_as_read(recipient=self.request.user)

        self.get_workflow_instance().do_transition(
            comment=form.cleaned_data['comment'], transition=transition,
            user=self.request.user
        )

        for old_tag in document.attached_tags().all():
            old_tag.documents.remove(document)
        transition.tag.documents.add(document)


        for follower_user in followers(transition):
            notify.send(sender=self.request.user, recipient=follower_user, verb="has made a workflow transition",
                action_object=transition, target=document)

        #added workflow transition as an event for the document object
        event_document_state_transitioned.commit(actor=self.request.user, target=document)

        return HttpResponseRedirect(self.get_success_url())

    def get_extra_context(self):
        return {
            'navigation_object_list': ('object', 'workflow_instance'),
            'object': self.get_workflow_instance().document,
            'submit_label': _('Submit'),
            'title': _(
                'Do transition for workflow: %s'
            ) % self.get_workflow_instance(),
            'workflow_instance': self.get_workflow_instance(),
        }

    def get_form_kwargs(self):
        kwargs = super(WorkflowInstanceTransitionView, self).get_form_kwargs()
        kwargs['workflow'] = self.get_workflow_instance()
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        return self.get_workflow_instance().get_absolute_url()

    def get_workflow_instance(self):
        return get_object_or_404(WorkflowInstance, pk=self.kwargs['pk'])


# Setup

class SetupWorkflowListView(SingleObjectListView):
    extra_context = {
        'title': _('Workflows'),
        'hide_link': True,
    }
    model = Workflow
    view_permission = permission_workflow_view


class SetupWorkflowCreateView(SingleObjectCreateView):
    form_class = WorkflowForm
    model = Workflow
    view_permission = permission_workflow_create
    post_action_redirect = reverse_lazy('document_states:setup_workflow_list')


class SetupWorkflowEditView(SingleObjectEditView):
    form_class = WorkflowForm
    model = Workflow
    view_permission = permission_workflow_edit
    post_action_redirect = reverse_lazy('document_states:setup_workflow_list')


class SetupWorkflowDeleteView(SingleObjectDeleteView):
    model = Workflow
    view_permission = permission_workflow_delete
    post_action_redirect = reverse_lazy('document_states:setup_workflow_list')

    def delete(self, request, *args, **kwargs):
        workflow = get_object_or_404(Workflow, pk=self.kwargs['pk'])
        if workflow.document_types.count():
            messages.error(
                self.request, _('A workflow cannot be deleted when document types are associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        if WorkflowInstance.objects.filter(workflow=workflow).count():
            messages.error(
                self.request, _('A workflow cannot be deleted when there are documents associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        return super(SetupWorkflowDeleteView, self).delete(request, *args, **kwargs)


class SetupWorkflowDocumentTypesView(AssignRemoveView):
    decode_content_type = True
    object_permission = permission_workflow_edit
    left_list_title = _('Available document types')
    right_list_title = _('Document types assigned this workflow')

    def check_workflow_sanity(self):
        #Every workflow should have an initial and final state defined
        try:
            initial_state = self.workflow.states.get(initial=True)
            final_state = self.workflow.states.get(final=True)
        except ObjectDoesNotExist:
            return (True, "Workflow should have both an initial and a final state")

        #Check for connectivity from initial to final state and it should pass through all states else the workflow integrity fails
        graph = {}
        states_list = self.workflow.states.all()
        for state in states_list:
            t_list = self.workflow.transitions.filter(origin_state=state)
            dest_states_list = []
            for t in t_list:
                dest_states_list.append(t.destination_state)
            graph[state] = dest_states_list

        states_set = set()
        for s in states_list:
            states_set.add(s)

        paths = self.find_path(graph, start_state=initial_state, end_state=final_state)

        if paths:
            path_set = set()
            for state in paths:
                path_set.add(state)
            remaining_states_set = states_set.difference(path_set)

            if len(remaining_states_set) ==  0:
                return (False,"")

            disconnected_states = []
            while True:
                try:
                    new_start_state = remaining_states_set.pop()
                    if not self.find_path(graph, start_state=new_start_state, end_state=final_state):
                        disconnected_states.append(new_start_state)
                except KeyError:
                    if disconnected_states:
                        return (True, "Disconnected states in workflow: " + unicode(disconnected_states))
                    else:
                        #check if there are any documents in a state which is no more part of the workflow
                        #count = self.check_documents_in_unused_state(states_list)
                        count = 0
                        if count:
                            return (True, "There are " + count + " documents in states which are not part of the workflow. First transition the documents to states which are part of the workflow.")
                        else:
                            return (False, "")
        else:
            return (True, "No path from initial to final state")

    def find_path(self, graph, start_state, end_state, path=[]):
        path = path + [start_state]
        if start_state == end_state:
            return path
        if not graph.has_key(start_state):
            return None
        for node in graph[start_state]:
            if node not in path:
                newpath = self.find_path(graph, node, end_state, path)
                if newpath: return newpath
        return None

    def check_documents_in_unused_state(self, workflow_states):
        qs = WorkflowInstance.objects.filter(workflow=self.workflow)

        count = 0
        for wf_instance in qs:
            if not wf_instance.get_last_transition():
                pass
            elif wf_instance.get_last_transition().destination_state not in workflow_states:
                count += 1

        return count

    def add(self, item):
        self.workflow = self.get_object()

        err, err_message = self.check_workflow_sanity()
        if err:
            messages.error(
                self.request, _(err_message)
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        self.workflow.document_types.add(item)

        # TODO: add task launching this workflow for all the document types
        # of item

    def get_extra_context(self):
        return {
            'title': _(
                'Document types assigned the workflow: %s'
            ) % self.get_object(),
            'object': self.get_object(),
        }

    def get_object(self):
        return get_object_or_404(Workflow, pk=self.kwargs['pk'])

    def get_document_types_not_included_in_workflows(self):
        document_type_pks = set()
        wf_list = Workflow.objects.all()
        for wf in wf_list:
            wfd_list = wf.document_types.all()
            for d in wfd_list:
                document_type_pks.add(d.pk)
        return DocumentType.objects.exclude(pk__in=document_type_pks)

    def left_list(self):
        return AssignRemoveView.generate_choices(
            #self.get_object().get_document_types_not_in_workflow()
            self.get_document_types_not_included_in_workflows()
        )

    def right_list(self):
        return AssignRemoveView.generate_choices(
            self.get_object().document_types.all()
        )

    def remove(self, item):
        self.get_object().document_types.remove(item)
        # TODO: add task deleting this workflow for all the document types of
        # item


class SetupWorkflowStateListView(SingleObjectListView):
    def dispatch(self, request, *args, **kwargs):
        try:
            Permission.check_permissions(
                request.user, (permission_workflow_view,)
            )
        except PermissionDenied:
            AccessControlList.objects.check_access(
                permission_workflow_view, request.user, self.get_workflow()
            )

        return super(
            SetupWorkflowStateListView, self
        ).dispatch(request, *args, **kwargs)

    def get_extra_context(self):
        return {
            'hide_link': True,
            'object': self.get_workflow(),
            'title': _('States of workflow: %s') % self.get_workflow()
        }

    def get_queryset(self):
        return self.get_workflow().states.all()

    def get_workflow(self):
        return get_object_or_404(Workflow, pk=self.kwargs['pk'])


class SetupWorkflowStateCreateView(SingleObjectCreateView):
    form_class = WorkflowStateForm
    view_permission = permission_workflow_edit

    def get_extra_context(self):
        return {
            'object': self.get_workflow(),
            'title': _(
                'Create states for workflow: %s'
            ) % self.get_workflow()
        }

    def get_workflow(self):
        return get_object_or_404(Workflow, pk=self.kwargs['pk'])

    def get_queryset(self):
        return self.get_workflow().states.all()

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_states', args=(self.kwargs['pk'],)
        )

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.workflow = self.get_workflow()

        if self.object.workflow.document_types.count():
            messages.error(
            self.request, _('A state cannot be added to a workflow when document types are associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )
        if self.object.initial and self.object.final:
            messages.error(
            self.request, _('A state cannot be both initial and final')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        #forcing the first created state for a workflow to be an initial state
        if not self.object.initial:
            if not self.object.workflow.states.count():
                self.object.initial = True

        self.object.save()
        return super(SetupWorkflowStateCreateView, self).form_valid(form)


class SetupWorkflowStateDeleteView(SingleObjectDeleteView):
    model = WorkflowState
    view_permission = permission_workflow_edit

    def delete(self, request, *args, **kwargs):
        try:
            state_obj = WorkflowState.objects.get(pk=self.kwargs['pk'])
            if state_obj.initial:
                messages.error(
                request, _('Initial state cannot be deleted. Make another state as initial state and then try deleting this state.')
                )
                return HttpResponseRedirect(
                    request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
                )

            wfi_list = WorkflowInstance.objects.filter(workflow=state_obj.workflow)
            for wfi in wfi_list:
                if wfi.get_current_state() == state_obj:
                    messages.error(
                    request, _('This state cannot be deleted since there is atleast one document in this state')
                    )
                    return HttpResponseRedirect(
                        request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
                    )

            if WorkflowTransition.objects.filter(Q(origin_state=state_obj) | Q(destination_state=state_obj)).count() > 0:
                messages.error(
                request, _('This state cannot be deleted since it is part of a workflow transition')
                )
                return HttpResponseRedirect(
                    request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
                )

            else:
               return super(SetupWorkflowStateDeleteView, self).delete(request, *args, **kwargs)
        except Exception as e:
            messages.error(
                    request, _(
                        'Error deleting the state due to:  '
                        '%(error)s.'
                    ) % {
                        'error': e
                    }
                )

        return HttpResponseRedirect(
                request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
        )


    def get_extra_context(self):
        return {
            'navigation_object_list': ('object', 'workflow_instance'),
            'object': self.get_object(),
            'workflow_instance': self.get_object().workflow,
        }

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_states',
            args=(self.get_object().workflow.pk,)
        )


class SetupWorkflowStateEditView(SingleObjectEditView):
    form_class = WorkflowStateForm
    model = WorkflowState
    view_permission = permission_workflow_edit

    def get_extra_context(self):
        return {
            'navigation_object_list': ('object', 'workflow_instance'),
            'object': self.get_object(),
            'workflow_instance': self.get_object().workflow,
        }

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_states',
            args=(self.get_object().workflow.pk,)
        )

    def get_state(self):
        return get_object_or_404(WorkflowState, pk=self.kwargs['pk'])

    def form_valid(self, form):
        self.object = form.save(commit=False)

        if not self.object.initial:
            orig_state = self.get_state()
            if orig_state.initial:
                messages.error(
                    self.request, _('Form cannot be saved..Initial state check box has to be ticked.')
                )
                return HttpResponseRedirect(
                    self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
                )
        if self.object.initial and self.object.final:
            messages.error(
            self.request, _('A state cannot be both initial and final')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        return super(SetupWorkflowStateEditView, self).form_valid(form)

# Transitions

class SetupWorkflowTransitionListView(SingleObjectListView):
    view_permission = permission_workflow_view

    def get_workflow(self):
        return get_object_or_404(Workflow, pk=self.kwargs['pk'])

    def get_queryset(self):
        return self.get_workflow().transitions.all()

    def get_extra_context(self):
        return {
            'hide_link': True,
            'object': self.get_workflow(),
            'title': _(
                'Transitions of workflow: %s'
            ) % self.get_workflow()
        }


class SetupWorkflowTransitionCreateView(SingleObjectCreateView):
    form_class = WorkflowTransitionForm
    view_permission = permission_workflow_edit

    def get_extra_context(self):
        return {
            'object': self.get_workflow(),
            'title': _(
                'Create transitions for workflow: %s'
            ) % self.get_workflow()
        }

    def get_form_kwargs(self):
        kwargs = super(
            SetupWorkflowTransitionCreateView, self
        ).get_form_kwargs()
        kwargs['workflow'] = self.get_workflow()
        return kwargs

    def get_workflow(self):
        return get_object_or_404(Workflow, pk=self.kwargs['pk'])

    def get_queryset(self):
        return self.get_workflow().transitions.all()

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_transitions',
            args=(self.kwargs['pk'],)
        )

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.workflow = self.get_workflow()

        if self.object.workflow.document_types.count():
            messages.error(
                self.request, _('A transition cannot be added to a workflow when document types are associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        if self.object.origin_state == self.object.destination_state:
            messages.error(
                self.request, _('A origin state and destination state in a transition cannot be the same')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        if self.object.origin_state.final:
            messages.error(
                self.request, _('A origin state in a transition cannot be a final state')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        try:
            WorkflowTransition.objects.get(origin_state=self.object.origin_state, destination_state=self.object.destination_state)
            messages.error(
                self.request, _('There is already a transition which has the same origin and destination state')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )
        except ObjectDoesNotExist:
            pass

        try:
            self.object.save()
        except IntegrityError:
            messages.error(
                self.request, _('Unable to save transition; integrity error.')
            )
            return super(
                SetupWorkflowTransitionCreateView, self
            ).form_invalid(form)
        else:
            return HttpResponseRedirect(self.get_success_url())


class SetupWorkflowTransitionDeleteView(SingleObjectDeleteView):
    model = WorkflowTransition
    view_permission = permission_workflow_edit

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'navigation_object_list': ('object', 'workflow_instance'),
            'workflow_instance': self.get_object().workflow,
        }

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_transitions',
            args=(self.get_object().workflow.pk,)
        )

    def delete(self, request, *args, **kwargs):
        transition = get_object_or_404(WorkflowTransition, pk=self.kwargs['pk'])
        if transition.workflow.document_types.count():
            messages.error(
                self.request, _('A transition cannot be deleted in a workflow when document types are associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        return super(SetupWorkflowTransitionDeleteView, self).delete(request, *args, **kwargs)

class SetupWorkflowTransitionEditView(SingleObjectEditView):
    form_class = WorkflowTransitionForm
    model = WorkflowTransition
    view_permission = permission_workflow_edit

    def get_extra_context(self):
        return {
            'navigation_object_list': ('object', 'workflow_instance'),
            'object': self.get_object(),
            'workflow_instance': self.get_object().workflow,
        }

    def get_form_kwargs(self):
        kwargs = super(
            SetupWorkflowTransitionEditView, self
        ).get_form_kwargs()
        kwargs['workflow'] = self.get_object().workflow
        return kwargs

    def get_success_url(self):
        return reverse(
            'document_states:setup_workflow_transitions',
            args=(self.get_object().workflow.pk,)
        )

    def get_workflow(self):
        transition = get_object_or_404(WorkflowTransition, pk=self.kwargs['pk'])
        return transition.workflow

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.workflow = self.get_workflow()

        if self.object.workflow.document_types.count():
            messages.error(
            self.request, _('A transition cannot be edited in a workflow when document types are associated with the workflow')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        if self.object.origin_state == self.object.destination_state:
            messages.error(
            self.request, _('A origin state and destination state in a transition cannot be the same')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )


        if self.object.origin_state.final:
            messages.error(
            self.request, _('A origin state in a transition cannot be a final state')
            )
            return HttpResponseRedirect(
                self.request.META.get('HTTP_REFERER', reverse(settings.LOGIN_REDIRECT_URL))
            )

        return super(SetupWorkflowTransitionEditView, self).form_valid(form)


class SetupWorkflowTransitionRoleMembersView(AssignRemoveView):
    model = WorkflowTransition
    grouped = False
    object_permission = permission_workflow_edit
    left_list_title = _('Available users')
    right_list_title = _('Workflow transition users')

    def add(self, item):
        user = get_object_or_404(User, pk=item)
        self.get_object().users.add(user)

        #User is made to follow the current transition
        transition = self.get_object()
        #follow(user, transition, actor_only=False)

        #user has to follow other transitions which are associated with this transition
        wt_list = WorkflowTransition.objects.filter(workflow=transition.workflow, origin_state=transition.destination_state)
        for wt in wt_list:
            follow(user, wt, actor_only=False)

        wt_list = WorkflowTransition.objects.filter(workflow=transition.workflow, destination_state=transition.origin_state)
        for wt in wt_list:
            follow(user, wt, actor_only=False)


    def get_extra_context(self):
        return {
            'title': _(
                'Members assigned to the workflow transition: %s'
            ) % self.get_object(),
            'object': self.get_object(),
        }

    def get_object(self):
        return get_object_or_404(WorkflowTransition, pk=self.kwargs['pk'])

    def left_list(self):
        return [
            (unicode(user.pk), user.username) for user in set(User.objects.all()) - set(self.get_object().users.all())
        ]

    def right_list(self):
        return [
            (unicode(user.pk), user.username) for user in self.get_object().users.all()
        ]

    def remove(self, item):
        user = get_object_or_404(User, pk=item)
        self.get_object().users.remove(user)

        transition = self.get_object()
        #unfollow(user, transition)

        #user has to unfollow other transitions which are associated with him
        wt_list = WorkflowTransition.objects.filter(workflow=transition.workflow, origin_state=transition.destination_state)
        for wt in wt_list:
            unfollow(user, wt)

        wt_list = WorkflowTransition.objects.filter(workflow=transition.workflow, destination_state=transition.origin_state)
        for wt in wt_list:
            unfollow(user, wt)


