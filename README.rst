|Build Status| |Coverage badge| |PyPI badge| |Installs badge| |License badge|

|Logo|

Description
-----------

This is a forked version of Mayan EDMS 2.0 version. This forked version mainly was created to make workflows in Mayan a practicable thing.
Currently Mayan does not have a full fledged workflow feature (although Roberto has put that in his future roadmap).

This fork might be useful for organizations who would want to setup workflows to manage their documents.

Please feel free to create a issue ticket here if you come across any issues.

`Video demostration`_

Main changes in this forked version
--------------------------------------
Users can be now added to workflow transitions so that users get notifications when a workflow transition happens.
State integrity is checked when a workflow is created.
Tags are linked to a transition. When a transition is made on a document, the tag correspondingly would change on the document which will highlight the status of the document.
A document can be uploaded by a user only if he has access to view that document type.
Notifications are shown on top right hand corner of the screen when workflow transitions are made.
Notifications are only shown to users who are part of the workflow transitions, and not to all the users.
Bug fixes.

Installation
------------

To try out this forked version of **Mayan EDMS**, simply do:

.. code-block:: bash

    $ virtualenv venv
    $ source venv/bin/activate
    $ git clone git@gitlab.com:ammaranjith/mayan-edms.git
    $ cd mayan-edms
    $ git checkout development
    $ pip install -r requirements.txt --timeout 1500
    $ python manage.py initialsetup
    $ mayan-edms.py runserver

Point your browser to 127.0.0.1:8000 and use the automatically created admin
account.

TODOs
------
Write Unit Tests for changes made.
Include Selenium tests for comprehensive testing.


.. _Website: http://www.mayan-edms.com
.. _Video demostration: http://upayogsolutions.com/edms_features_demos
.. _Documentation: http://readthedocs.org/docs/mayan/en/latest/
.. _Translations: https://www.transifex.com/projects/p/mayan-edms/
.. _Mailing list (via Google Groups): http://groups.google.com/group/mayan-edms
.. _Apache 2.0 License: https://www.apache.org/licenses/LICENSE-2.0.txt
.. _`the repository`: http://gitlab.com/mayan-edms/mayan-edms
.. _`contributors file`: https://gitlab.com/mayan-edms/mayan-edms/blob/master/docs/topics/contributors.rst

.. |Build Status| image:: https://gitlab.com/ci/projects/6169/status.png?ref=master
   :target: https://gitlab.com/ci/projects/6169?ref=master
.. |Logo| image:: https://gitlab.com/mayan-edms/mayan-edms/raw/master/docs/_static/mayan_logo.png
.. |Animation| image:: https://gitlab.com/mayan-edms/mayan-edms/raw/master/docs/_static/overview.gif
.. |Installs badge| image:: http://img.shields.io/pypi/dm/mayan-edms.svg?style=flat
   :target: https://crate.io/packages/mayan-edms/
.. |PyPI badge| image:: http://img.shields.io/pypi/v/mayan-edms.svg?style=flat
   :target: http://badge.fury.io/py/mayan-edms
.. |License badge| image:: http://img.shields.io/badge/license-Apache%202.0-green.svg?style=flat
.. |Analytics| image:: https://ga-beacon.appspot.com/UA-52965619-2/mayan-edms/readme?pixel
.. |Coverage badge| image:: https://codecov.io/gitlab/mayan-edms/mayan-edms/coverage.svg?branch=master
   :target: https://codecov.io/gitlab/mayan-edms/mayan-edms?branch=master

|Analytics|
